This is a testharness.js-based test.
PASS Headers.prototype.get combines set-cookie headers in order
FAIL Headers iterator does not combine set-cookie headers assert_equals: Array length is not equal expected 2 but got 1
PASS Headers iterator does not special case set-cookie2 headers
FAIL Headers iterator does not combine set-cookie & set-cookie2 headers assert_equals: Array length is not equal expected 3 but got 2
FAIL Headers iterator preserves set-cookie ordering assert_equals: Array length is not equal expected 3 but got 1
FAIL Headers iterator preserves per header ordering, but sorts keys alphabetically assert_equals: Array length is not equal expected 5 but got 4
FAIL Headers iterator preserves per header ordering, but sorts keys alphabetically (and ignores value ordering) assert_equals: Array length is not equal expected 5 but got 4
FAIL Headers iterator is correctly updated with set-cookie changes assert_array_equals: value is undefined, expected array
PASS Headers.prototype.has works for set-cookie
FAIL Headers.prototype.append works for set-cookie assert_equals: Array length is not equal expected 3 but got 2
PASS Headers.prototype.set works for set-cookie
PASS Headers.prototype.delete works for set-cookie
FAIL Headers.prototype.getSetCookie with no headers present headers.getSetCookie is not a function
FAIL Headers.prototype.getSetCookie with one header headers.getSetCookie is not a function
FAIL Headers.prototype.getSetCookie with one header created from an object headers.getSetCookie is not a function
FAIL Headers.prototype.getSetCookie with multiple headers headers.getSetCookie is not a function
FAIL Headers.prototype.getSetCookie with an empty header headers.getSetCookie is not a function
FAIL Headers.prototype.getSetCookie with two equal headers headers.getSetCookie is not a function
FAIL Headers.prototype.getSetCookie ignores set-cookie2 headers headers.getSetCookie is not a function
FAIL Headers.prototype.getSetCookie preserves header ordering headers.getSetCookie is not a function
FAIL Set-Cookie is a forbidden response header response.headers.getSetCookie is not a function
Harness: the test ran to completion.

